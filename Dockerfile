FROM php:8-fpm-alpine

ARG ENABLE_GIT=n

RUN apk update && \
  apk upgrade && \
  if [ "$ENABLE_GIT" = "y" ] ; then \
    apk add --no-cache git=~2.38.4; \
  fi

CMD ["php-fpm"]
